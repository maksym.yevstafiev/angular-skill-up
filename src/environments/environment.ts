// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  KENDO_UI_LICENSE: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkxJQyJ9.eyJwcm9kdWN0cyI6W3sidHJpYWwiOnRydWUsImNvZGUiOiJLRU5ET1VJQU5HVUxBUiIsImxpY2Vuc2VFeHBpcmF0aW9uRGF0ZSI6MTY0MjcyNjU3Mn1dLCJpbnRlZ3JpdHkiOiJUTjJUK0xIRlNHSDF5N2VhV04zR1VJMlwveDEwPSIsImxpY2Vuc2VIb2xkZXIiOiJtYWtzeW0ueWV2c3RhZmlldkBnbWFpbC5jb20iLCJpYXQiOjE2NDAyNzE0ODQsImF1ZCI6Im1ha3N5bS55ZXZzdGFmaWV2QGdtYWlsLmNvbSJ9.odYMHVcxBSyvehlq3tuAqRaGKBBe99YHhDpC9EWSUp2U4KVu4UKyb-kaDvkXlEVBcTyi-gJ6sa3dz7WBk4eF4A5HNOkMmArWDOdHC31-Gcrtum3E_p_fAJEmzjdlmnyKV9tkateL7PsBUkTb1v21kIaaO0w3-OtWutK3neTXd73nDW4EM992Da5lGxhpZ5-JqaUN49awxmKx9GM7728mTsk_06tO4SUf7j9R2dAvokTvgJ7ktWwnnLWNY-p5j4EEhvXhgPFrezZKLjiUmjuj9GxQ5S1wNbTxuhpe8PzKady0ZU28Zk3jF49QwOFP2fiBRCLK12iNrgEQS6H33_jekw'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
