import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {UsersModule} from "./modules/users/users.module"
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ExcelModule, GridModule, PDFModule,} from "@progress/kendo-angular-grid";
import {InputsModule} from '@progress/kendo-angular-inputs';
import {ChartsModule} from "@progress/kendo-angular-charts";
import 'hammerjs';
import {DropDownsModule} from "@progress/kendo-angular-dropdowns";
import {UserHelper} from "./modules/users/services/helpers/user-helper";
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { IconsModule } from '@progress/kendo-angular-icons';



@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UsersModule,
    GridModule,
    BrowserAnimationsModule,
    GridModule,
    ChartsModule,
    InputsModule,
    PDFModule,
    ExcelModule,
    DropDownsModule,
    DialogsModule,
    IconsModule
  ],
  providers: [
    BrowserModule,
    AppRoutingModule,
    UserHelper,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
