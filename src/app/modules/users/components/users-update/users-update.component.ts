import {Component, Input, OnInit} from '@angular/core';
import {DropdownItemValue} from "../../models/interfaces/dropdown-item";
import {FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {User} from "../../models/interfaces/user";
import {UserHelper} from "../../services/helpers/user-helper";
import {UsersDataService} from "../../services/users-data.service";
import {WindowRef} from "@progress/kendo-angular-dialog";
import {GenderTypeList} from "../../models/enums/gender-type";
import {StudyCategoriesList, StudyCategoryEnum} from "../../models/enums/study-category";

@Component({
  selector: 'app-users-update',
  templateUrl: './users-update.component.html',
  styleUrls: ['./users-update.component.css']
})
export class UsersUpdateComponent implements OnInit {
  @Input() userToUpdate!: User;
  @Input() userRowId!: number;

  public min: Date = new Date(1971, 1, 1,0,0);
  public studyCategoriesList: DropdownItemValue[] = [];
  public genderTypeList: DropdownItemValue[] = [];
  public updateForm: FormGroup = this.buildFormGroup();

  public studyStartLimit: Date = new Date();
  public studyEndLimit: Date = new Date();
  public birthDateLimit: Date = this.min;

  private userNameToCheck: string = '';
  private studyCategory: string = '';
  private userToAdd!: User;

  constructor(
    private userHelper: UserHelper,
    private usersDataService: UsersDataService,
    private window : WindowRef,
  ) {
  }

  ngOnInit(): void {
    this.genderTypeList = this.userHelper.buildValuesForDropdown(GenderTypeList);
    this.studyCategoriesList = this.userHelper.buildValuesForDropdown(StudyCategoriesList);
    if (this.userRowId !== undefined){
      this.initFormWithUser();
    }
    this.initFormSubscription();
  }

  public submitNewUser(): void {
    this.updateForm.markAllAsTouched();
    this.usersDataService.addUser(this.userToAdd);
    this.window.close();
  }

  public updateUser(): void {
    this.updateForm.markAllAsTouched();
    this.usersDataService.updateUser(this.userRowId, this.userToUpdate);
    this.window.close();
  }

  public clearForm(): void {
    this.updateForm.reset();
  }

  private buildFormGroup(): FormGroup{
    return new FormGroup({
      userName: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(15)
        ], this.usersDataService.usernameValidator()
      ),
      gender: new FormControl('',Validators.required),
      birthDate: new FormControl(
        '', [
          Validators.required,
        ]
      ),
      studyCategory: new FormControl(
        '', [Validators.required]
      ),
      studyStart: new FormControl(
        '', [
          Validators.required,
        ]
      ),
      studyEnd: new FormControl(
        '',
      ),
    });
  }

  private initFormWithUser(): void {
    let gender = this.genderTypeList.find( g => g.text == this.userToUpdate.gender);
    let studyCategory = this.studyCategoriesList.find( c => c.text == this.userToUpdate.studyCategory);
    this.updateForm.setValue({
      userName: this.userToUpdate.userName,
      gender: gender,
      birthDate: this.userToUpdate.birthDate,
      studyCategory: studyCategory,
      studyStart: this.userToUpdate.studyStart,
      studyEnd: this.userToUpdate.studyEnd
    });
  }

  private initFormSubscription(): void {
    this.updateForm.valueChanges.subscribe((v)=>{
      const user = v;

      user.gender = v.gender.text;
      user.studyCategory = v.studyCategory.text;

      this.userNameToCheck = user.userName;
      this.studyStartLimit = user.studyStart;
      this.studyEndLimit = user.studyEnd;
      this.birthDateLimit = user.birthDate;
      const validatorsRequired = user.studyCategory !== StudyCategoryEnum.backend && user.studyCategory !== StudyCategoryEnum.frontend

      if(this.userRowId !== undefined){
        this.userToUpdate = user;
      } else {
        this.userToAdd = user;
      }

      if (validatorsRequired){
        this.updateForm.controls['studyEnd'].addValidators(Validators.required)
      } else {
        this.updateForm.controls['studyEnd'].removeValidators(Validators.required);
        this.updateForm.updateValueAndValidity({emitEvent: false});
      }
    })
  }
}
