import {Component, OnDestroy, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {DataBindingDirective} from "@progress/kendo-angular-grid";
import {User} from "../../models/interfaces/user";
import {GenderTypeEnum, GenderTypeList} from "../../models/enums/gender-type";
import {StudyCategoriesList, StudyCategoryEnum} from "../../models/enums/study-category";
import {process, State} from "@progress/kendo-data-query";
import {UsersDataService} from "../../services/users-data.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit, OnDestroy {
  @ViewChild(DataBindingDirective)
  dataBinding!: DataBindingDirective;
  @Output() editUserEvent = new EventEmitter<{ user: User, idx: number }>()
  public users: User[] = [];
  public gridView!: any[];
  public mySelection: string[] = [];
  public columns:any = [
    { field: "userName", title: 'Имя пользователя' },
    { field: "gender", title: 'Пол' },
    { field: "birthDate", title: 'Дата рождения', format: 'dd/MM/yyyy'},
    { field: "studyCategory", title: 'Направление обучения' },
    { field: "studyStart", title: 'Начало обучения', format: 'dd/MM/yyyy' },
    { field: "studyEnd", title: 'Конец обучения',  format: 'dd/MM/yyyy' },
  ];


  private userListSubscription!: Subscription;

  constructor(private usersDataService: UsersDataService) {
  }

  public ngOnInit(): void {
    this.userListSubscription = this.usersDataService.currentUserList.subscribe(
      (usersList) => {
        this.users = usersList
      }
    );
  }

  public onFilter(inputValue: string | Date | GenderTypeEnum | StudyCategoryEnum): void {
    this.gridView = process(this.users, {
      filter: {
        logic: "or",
        filters: [
          {
            field: "name",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "gender",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "birthDate",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "studyCategory",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "studyStart",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "studyEnd",
            operator: "contains",
            value: inputValue,
          },
        ],
      },
    }).data;
    this.dataBinding.skip = 0;
  }

  deleteUser(idx: number){
    this.usersDataService.deleteUserById(idx);
  }

  editUser(user: any, idx: number){
    this.editUserEvent.emit({user: user, idx: idx});
  }

  ngOnDestroy(): void {
    this.userListSubscription.unsubscribe();
  }
}
