import {DropdownItemValue} from "../../models/interfaces/dropdown-item";

export class UserHelper {
  public buildValuesForDropdown(o: any): DropdownItemValue[]{
    let dropdownsItems: DropdownItemValue[] = [];
    Object.getOwnPropertyNames(o).forEach((v,i)=>{
      dropdownsItems[i] = { value: v, text: o[v] };
    })
    return dropdownsItems
  }
}
