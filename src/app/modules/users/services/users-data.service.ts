import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of, map} from 'rxjs';
import {User} from "../models/interfaces/user";
import {AbstractControl, AsyncValidatorFn, ValidationErrors} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {
  private usersList = new BehaviorSubject(new Array<User>());
  currentUserList = this.usersList.asObservable();
  currentListValue: User[] = this.usersList.getValue();

  constructor() { }

  addUser(user: User){
    this.currentListValue.push(user);
    this.usersList.next(this.currentListValue);
  }

  updateUser(userRowId: number, userUpdateData: User){
    if (userRowId == 0){
      this.currentListValue[0] = userUpdateData;
    } else {
      this.currentListValue[this.currentListValue.length-userRowId] = userUpdateData;
    }
    this.usersList.next(this.currentListValue);
  }

  deleteUserById(userRowId: number){
    if (userRowId == 0){
      this.currentListValue.splice(userRowId,1);
    } else {
      this.currentListValue.splice(this.currentListValue.length-userRowId,1);
    }
    this.usersList.next(this.currentListValue);
  }

  private isNameUnique(name: string){
    return of(this.currentListValue.some(u => u.userName == name));
  }

  usernameValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.isNameUnique(control.value).pipe(
        map(res => {
          return res ? { usernameExists: true } : null;
        })
      );
    };
  }
}
