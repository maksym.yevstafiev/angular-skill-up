import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersComponent} from './users.component';
import {UsersTableComponent} from "./components/users-table/users-table.component";
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "../../app-routing.module";
import {GridModule} from "@progress/kendo-angular-grid";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {DialogsModule, WindowModule} from "@progress/kendo-angular-dialog";
import {ButtonsModule} from "@progress/kendo-angular-buttons";
import {DateInputsModule} from "@progress/kendo-angular-dateinputs";
import {LabelModule} from "@progress/kendo-angular-label";
import {InputsModule} from "@progress/kendo-angular-inputs";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DropDownsModule, SharedModule} from "@progress/kendo-angular-dropdowns";
import { UsersUpdateComponent } from './components/users-update/users-update.component';
import { IconsModule } from "@progress/kendo-angular-icons";

@NgModule({
  declarations: [
    UsersComponent,
    UsersTableComponent,
    UsersUpdateComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    GridModule,
    BrowserAnimationsModule,
    DialogsModule,
    ButtonsModule,
    DateInputsModule,
    LabelModule,
    InputsModule,
    SharedModule,
    DropDownsModule,
    WindowModule,
    IconsModule
  ]
})
export class UsersModule {
}
