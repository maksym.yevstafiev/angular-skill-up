import {StudyCategoriesList, StudyCategoryEnum} from "../enums/study-category";
import {GenderTypeEnum, GenderTypeList} from "../enums/gender-type";

export interface User {
  userName: string;
  gender: GenderTypeEnum;
  birthDate: Date;
  studyCategory: StudyCategoryEnum;
  studyStart: Date;
  studyEnd: Date;
}
