export interface DropdownItemValue {
  value: string,
  text: string
}
