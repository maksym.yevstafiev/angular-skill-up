export enum GenderTypeEnum {
  male = 'Male',
  female = 'Female'
}

export const GenderTypeList = {
  male: GenderTypeEnum.male,
  female: GenderTypeEnum.female
}
