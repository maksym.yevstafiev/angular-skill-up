export enum StudyCategoryEnum {
  backend = 'Backend',
  frontend = 'Frontend',
  design = 'Design',
  projectManagement = 'Project Management',
  qualityAssurance = 'Quality Assurance',
  businessAnalytic = 'Business Analytic'
}

export const StudyCategoriesList = {
  backend:  StudyCategoryEnum.backend,
  frontend:  StudyCategoryEnum.frontend,
  design:  StudyCategoryEnum.design,
  projectManagement:  StudyCategoryEnum.projectManagement,
  qualityAssurance:  StudyCategoryEnum.qualityAssurance,
  businessAnalytic: StudyCategoryEnum.businessAnalytic
}
