import {Component, OnInit} from '@angular/core';
import {WindowRef, WindowService} from "@progress/kendo-angular-dialog";
import {UsersUpdateComponent} from "./components/users-update/users-update.component";
import {User} from "./models/interfaces/user";


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  private windowRef!: WindowRef;

  constructor( private windowService: WindowService ) {
  }

  ngOnInit(): void {
  }

  openAddUserModal(){
    this.windowRef = this.getUserUpdateModalRef()
  }

  openUpdateUserModal(userToUpdate: User, idx: number){
    this.windowRef = this.getUserUpdateModalRef();
    this.windowRef.content.instance.userToUpdate = userToUpdate
    this.windowRef.content.instance.userRowId = idx
  }

  getUserUpdateModalRef(): WindowRef{
    return this.windowService.open({
      title: "Добавление нового пользователя",
      content: UsersUpdateComponent,
      width: 480,
    });
  }
}
